﻿using taskMail.Models;

namespace taskMail.Services.EmailService
{
    public interface IEmailService
    {
        void sendEmail(DailyNotes request);
    }
}
