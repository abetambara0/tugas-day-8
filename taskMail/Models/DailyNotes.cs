﻿namespace taskMail.Models
{
    public class DailyNotes
    {
        public string emailTo { get; set; } = string.Empty;
        //public string Subject { get; set; } = string.Empty;
        public string[] notes { get; set; } = new string[0];
    }
}
