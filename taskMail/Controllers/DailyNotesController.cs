﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using MimeKit.Text;
using taskMail.Models;

namespace taskMail.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DailyNotesController : ControllerBase
    {
        private readonly IEmailService _emailService;
        public DailyNotesController(IEmailService emailService)
        {
            _emailService = emailService;
        }
        [HttpPost]
        public IActionResult SendDailyNotes(DailyNotes dailyNotes)
        {
            _emailService.sendEmail(dailyNotes);
            return Ok(dailyNotes.notes);
        }
    }
}
